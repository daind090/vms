/**
 * View Models used by Spring MVC REST controllers.
 */
package com.test.vms.web.rest.vm;
